package main

import (
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"learn_fiber/config"
	"learn_fiber/routes"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	config.PGSQL(
		config.PutEnv("DB_HOST"),
		config.PutEnv("DB_PORT"),
		config.PutEnv("DB_USER"),
		config.PutEnv("DB_PASSWORD"),
		config.PutEnv("DB_NAME"))

	app := fiber.New()

	routes.UserRoutes(app)

	app.Use(cors.New())

	app.Use(logger.New(logger.Config{
		Format:   "[${ip}]:${port} ${status} - ${method} ${path}\n",
		TimeZone: "Asia/Jakarta",
	}))

	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404) // => 404 "Not Found"
	})

	log.Fatal(app.Listen(":3000"))
}
