package routes

import (
	"github.com/gofiber/fiber/v2"
	"learn_fiber/controllers"
)

func UserRoutes(app *fiber.App) {
	app.Get("/users", controllers.GetUsers)
	app.Get("/user/:id", controllers.GetUser)
	app.Post("/user/store", controllers.StoreUser)
	app.Post("/user/update", controllers.UpdateUser)
	app.Post("/user/delete", controllers.DeleteUser)
}
