package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
)

func PutEnv(key string) string {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal("Error loading .env file")
	}

	return os.Getenv(key)
}
