package main

import (
	"learn_fiber/config"
	"learn_fiber/models"
)

func main() {
	config.PGSQL(
		config.PutEnv("DB_HOST"),
		config.PutEnv("DB_PORT"),
		config.PutEnv("DB_USER"),
		config.PutEnv("DB_PASSWORD"),
		config.PutEnv("DB_NAME"))

	config.Conn.Migrator().DropTable(&models.Users{})
	config.Conn.AutoMigrate(&models.Users{})
}
