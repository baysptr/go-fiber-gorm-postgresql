package controllers

import (
	"github.com/gofiber/fiber/v2"
	"learn_fiber/config"
	"learn_fiber/models"
)

func StoreUser(c *fiber.Ctx) error {
	user := new(models.Users)
	if err := c.BodyParser(user); err != nil {
		return c.Status(400).JSON(err.Error())
	}

	user.BeforeSave()

	config.Conn.Create(&user)

	return c.Status(201).JSON(user)
}

func GetUsers(c *fiber.Ctx) error {
	users := []models.Users{}
	config.Conn.Find(&users)

	exec := config.Conn.Find(&users)

	if exec.RowsAffected == 0 {
		return c.Status(404).JSON(exec.Error)
	}

	return c.Status(200).JSON(users)
}

func GetUser(c *fiber.Ctx) error {
	id := c.Params("id")
	user := models.Users{}

	exec := config.Conn.Find(&user, id)

	if exec.RowsAffected == 0 {
		return c.Status(404).JSON(exec.Error)
	}

	return c.Status(200).JSON(user)
}

func UpdateUser(c *fiber.Ctx) error {
	user := new(models.Users)

	if err := c.BodyParser(user); err != nil {
		return c.Status(503).JSON(err.Error())
	}

	user.BeforeSave()
	exec := config.Conn.Where("id=?", user.ID).Updates(&user)

	if exec.RowsAffected == 0 {
		return c.Status(404).JSON(exec.Error)
	}

	return c.Status(200).JSON(&user)
}

func DeleteUser(c *fiber.Ctx) error {
	user := new(models.Users)

	if err := c.BodyParser(user); err != nil {
		return c.Status(503).JSON(err.Error())
	}

	exec := config.Conn.Delete(&user, user.ID)

	if exec.RowsAffected == 0 {
		return c.Status(404).SendString("FALSE")
	}

	return c.Status(200).SendString("TRUE")
}
